/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import javax.swing.JList;
import javax.swing.TransferHandler;

/**
 *
 * @author Pedro Arthur
 */
public class MyListDropHandler extends TransferHandler {

    private final DragDropList targetList;

    public MyListDropHandler(final DragDropList targetList) {
        this.targetList = targetList;
    }

    @Override
    public boolean canImport(final TransferHandler.TransferSupport support) {
        
        if (!support.isDataFlavorSupported(TransferableItem.TRANSFER_ITEM_DATA_FLAVOR)) {
            return false;
        }
        
        JList.DropLocation dl = (JList.DropLocation) support.getDropLocation();
        
        return dl.getIndex() != -1;
    }
    
    private int getDropIndex(int toIndex, int fromIndex) {
        
        if(toIndex < fromIndex) {
            return toIndex;
        }
        int dropIndex = toIndex - 1;
        return dropIndex;
    }

    @Override
    public boolean importData(final TransferHandler.TransferSupport support) {
        
        if (!canImport(support)) {
            return false;
        }

        Transferable transferable = support.getTransferable();
        TransferItem transferItem;
        try {
            transferItem = (TransferItem) transferable
                    .getTransferData(TransferableItem.TRANSFER_ITEM_DATA_FLAVOR);
        } catch (UnsupportedFlavorException | IOException e) {
            return false;
        }
        
        //drop index
        JList.DropLocation dl = (JList.DropLocation) support.getDropLocation();
       
        int dropIndex = getDropIndex(dl.getIndex(), transferItem.getIndex());
        
        //removing transfer item index
        this.targetList.removeAt(transferItem.getIndex());
        //adding the transfer item to the drop index
        this.targetList.insertAt(transferItem.getData(), dropIndex);
        
        return true;
    }
}
