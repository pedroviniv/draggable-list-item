/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist;

import java.awt.Container;

/**
 *
 * @author pedro
 */
public class SelectedContext {
    
    private int selectedElementIndex;
    private Object selectedElement;
    private Container selectedElementContainer;
    
    public <T> T getData(Class<T> type) {
        return type.cast(this.selectedElement);
    }

    public static class Builder {

        private int selectedElementIndex;
        private Object selectedElement;
        private Container dataContainer;

        private Builder() {
        }

        public Builder selectedElementIndex(final int value) {
            this.selectedElementIndex = value;
            return this;
        }

        public Builder selectedElement(final Object value) {
            this.selectedElement = value;
            return this;
        }

        public Builder selectedElementContainer(final Container value) {
            this.dataContainer = value;
            return this;
        }

        public SelectedContext build() {
            return new io.github.kieckegard.studies.drag.and.drop.jlist.SelectedContext(selectedElementIndex, selectedElement, dataContainer);
        }
    }

    public static SelectedContext.Builder builder() {
        return new SelectedContext.Builder();
    }

    private SelectedContext(final int selectedElementIndex, final Object selectedElement, final Container dataContainer) {
        this.selectedElementIndex = selectedElementIndex;
        this.selectedElement = selectedElement;
        this.selectedElementContainer = dataContainer;
    }

    public int getSelectedElementIndex() {
        return selectedElementIndex;
    }

    public Object getSelectedElement() {
        return selectedElement;
    }

    public Container getDataContainer() {
        return selectedElementContainer;
    }

    @Override
    public String toString() {
        return "RenameContext{" + "selectedElementIndex=" + selectedElementIndex + ", selectedElement=" + selectedElement + ", dataContainer=" + selectedElementContainer + '}';
    }
}
