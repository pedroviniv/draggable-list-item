/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 *
 * @author pedro
 */
public class TransferableItem implements Transferable {

    public static final DataFlavor TRANSFER_ITEM_DATA_FLAVOR = new DataFlavor(TransferItem.class, "java/TransferItem");
    private final TransferItem transferItem;

    public TransferableItem(final TransferItem transferItem) {
        this.transferItem = transferItem;
    }
    
    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { TRANSFER_ITEM_DATA_FLAVOR };
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor.equals(TRANSFER_ITEM_DATA_FLAVOR);
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return this.transferItem;
    }

    public TransferItem getTransferItem() {
        return transferItem;
    }
}
