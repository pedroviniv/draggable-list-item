/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist;


import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JList;

public abstract class DragDropList<T> extends JList<T> {

    private DefaultListModel model;
    private DragDropListListener dragDropListener;
    private SelectedItemHandler dataRenaming;

    public DragDropList(SelectedItemHandler selectedItemHandler) {
        
        super(new DefaultListModel());
        
        this.model = (DefaultListModel) getModel();
        this.dragDropListener = new DragDropListListener(this);
        
        setDragEnabled(true);
        setDropMode(DropMode.INSERT);

        super.setTransferHandler(new MyListDropHandler(this));
        
        this.dataRenaming = selectedItemHandler;
        this.initDefaultElements();
        this.initDoubleClickEvent();
    }
    
    private void initDefaultElements() {
        
        List<Object> defaultElements = this.getDefaultElements();
        defaultElements.forEach((elem) -> {
            this.model.addElement(elem);
        });
    }
    
    protected abstract List<Object> getDefaultElements();
    
    private void initDoubleClickEvent() {
        this.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                dragDropListMouseClicked(event);
            }
        });
    }
    
    private void dragDropListMouseClicked(MouseEvent event) {
        
        Rectangle r = this.getCellBounds(0, this.getLastVisibleIndex());
        
        if (r != null && r.contains(event.getPoint()) 
                && event.getClickCount() == 2) {
            
            int index = this.locationToIndex(event.getPoint());
            Object dataAtIndex = this.get(index, Object.class);
            
            SelectedContext renameContext = SelectedContext.builder()
                    .selectedElementIndex(index)
                    .selectedElement(dataAtIndex)
                    .selectedElementContainer(this)
                    .build();
            
            this.dataRenaming.handle(renameContext);
        }
    }

    public DragDropListListener getDragDropListener() {
        return dragDropListener;
    }
    
    public int indexOf(Object data) {
        return this.model.indexOf(data);
    }
    
    public void removeAt(int index) {
        this.model.remove(index);
    }
    
    public void clear() {
        this.model.clear();
    }
    
    public void setElements(List<Object> objects) {
        this.clear();
        this.addAll(objects);
    }
    
    public void setElements(Object... objects) {
        this.clear();
        this.addAll(objects);
    }
    
    public void add(Object object) {
        this.model.addElement(object);
    }
    
    public void addAll(Object... objects) {
        for(Object o : objects) {
            this.add(o);
        }
    }
    
    public void addAll(List<Object> objects) {
        objects.forEach((o) -> {
            this.add(o);
        });
    }
    
    public void insertAt(Object data, int index) {
        this.model.insertElementAt(data, index);
    }
    
    public <T> T get(int index, Class<T> type) {
        Object data = this.model.get(index);
        return type.cast(data);
    }
    
    public void toggleSelectAll(boolean select) {

        if (select) {

            int start = 0;
            int end = model.getSize() - 1;
            if (end >= 0) {
                this.setSelectionInterval(start, end);
            }
        } else {
            this.clearSelection();
        }
    }
    
    private Integer getSelectedItensCount() {

        int[] indices = this.getSelectedIndices();
        return indices.length;
    }
    
    public boolean hasSelectedItens() {
        return this.getSelectedItensCount() > 0;
    }
    
    public <T> List<T> getSelectedItems(Class<T> type) {
        int[] selectedIndices = this.getSelectedIndices();
        List<T> selectedItens = new ArrayList<>();
        for(int index : selectedIndices) {
            T item = this.get(index, type);
            selectedItens.add(item);
        }
        return selectedItens;
    }
    
    public <T> List<T> getAllItems(Class<T> type) {
        List<T> itens = new ArrayList<>();
        int size = this.model.getSize();
        for(int i = 0; i < size; i++) {
            itens.add(this.get(i, type));
        }
        return itens;
    }
}


