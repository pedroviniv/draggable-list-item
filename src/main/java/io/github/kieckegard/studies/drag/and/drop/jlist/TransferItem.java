/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist;

/**
 *
 * @author pedro
 */
public class TransferItem {
    
    private int index;
    private Object data;
    
    public <T> T getData(Class<T> type) {
        return type.cast(this.data);
    }

    public static class Builder {

        private int index;
        private Object data;

        private Builder() {
        }

        public Builder index(final int value) {
            this.index = value;
            return this;
        }

        public Builder data(final Object value) {
            this.data = value;
            return this;
        }

        public TransferItem build() {
            return new io.github.kieckegard.studies.drag.and.drop.jlist.TransferItem(index, data);
        }
    }

    public static TransferItem.Builder builder() {
        return new TransferItem.Builder();
    }

    private TransferItem(final int index, final Object data) {
        this.index = index;
        this.data = data;
    }

    public int getIndex() {
        return index;
    }

    public Object getData() {
        return data;
    }

    @Override
    public String toString() {
        return "TransferItem{" + "index=" + index + ", data=" + data + '}';
    }
}
