/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist.demo;

/**
 *
 * @author pedro
 */
public class Item {
    
    private String titulo;
    private Object data;

    public static class Builder {

        private String titulo;
        private Object data;

        private Builder() {
        }

        public Builder titulo(final String value) {
            this.titulo = value;
            return this;
        }

        public Builder data(final Object value) {
            this.data = value;
            return this;
        }

        public Item build() {
            return new io.github.kieckegard.studies.drag.and.drop.jlist.demo.Item(titulo, data);
        }
    }

    public static Item.Builder builder() {
        return new Item.Builder();
    }

    private Item(final String titulo, final Object data) {
        this.titulo = titulo;
        this.data = data;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return this.titulo;
    }
}
