/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist.demo;

import io.github.kieckegard.studies.drag.and.drop.jlist.SelectedContext;
import java.awt.Container;
import javax.swing.JOptionPane;
import io.github.kieckegard.studies.drag.and.drop.jlist.SelectedItemHandler;

/**
 *
 * @author Pedro Arthur
 */
public class SelectedItemRenameHandler implements SelectedItemHandler {
    
    private Container container;

    public SelectedItemRenameHandler(Container container) {
        this.container = container;
    }
    
    public SelectedItemRenameHandler() {}

    @Override
    public void handle(SelectedContext renameContext) {
        
        Item item = renameContext.getData(Item.class);

        String dialogTitle = this.getRenameDialogTitle(item);
        String selectedDataTitle = this.getSelectedDataTitle(item);
        String newDataTitle = JOptionPane.showInputDialog(this.container, 
                dialogTitle, selectedDataTitle);

        this.updateNewDataTitle(item, newDataTitle);
    }

    protected String getRenameDialogTitle(Item data) {
        return "Renomear título do item";
    }

    protected String getSelectedDataTitle(Item data) {
        return data.getTitulo();
    }

    protected void updateNewDataTitle(Item data, String newTitle) {
        data.setTitulo(newTitle);
    }
}
