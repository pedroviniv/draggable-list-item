/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist;

/**
 *
 * @author pedro
 */
public interface SelectedItemHandler {

    void handle(SelectedContext renameContext);
    
}
