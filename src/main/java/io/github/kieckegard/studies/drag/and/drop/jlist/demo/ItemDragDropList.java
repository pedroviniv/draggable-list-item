/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist.demo;

import io.github.kieckegard.studies.drag.and.drop.jlist.DragDropList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedro
 */
public class ItemDragDropList extends DragDropList {

    public ItemDragDropList() {
        super(new SelectedItemRenameHandler());
    }

    @Override
    protected List<Object> getDefaultElements() {
        
        List<Object> data = new ArrayList<>();
        
        Item jogo1 = Item.builder().titulo("Jogo #1").data(1L).build();
        Item jogo2 = Item.builder().titulo("Jogo #2").data(2L).build();
        Item jogo3 = Item.builder().titulo("Jogo #3").data(3L).build();
        Item jogo4 = Item.builder().titulo("Jogo #4").data(4L).build();
        Item jogo5 = Item.builder().titulo("Jogo #5").data(5L).build();
        Item jogo6 = Item.builder().titulo("Jogo #6").data(6L).build();
        
        data.add(jogo1);
        data.add(jogo2);
        data.add(jogo3);
        data.add(jogo4);
        data.add(jogo5);
        data.add(jogo6);
        
        return data;
    }

}
