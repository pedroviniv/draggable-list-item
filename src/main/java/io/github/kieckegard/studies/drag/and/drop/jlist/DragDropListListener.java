/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.studies.drag.and.drop.jlist;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;

/**
 *
 * @author pedro
 */
public class DragDropListListener implements DragSourceListener, DragGestureListener {

    private final DragDropList list;
    private final DragSource ds = new DragSource();

    public DragDropListListener(final DragDropList list) {
        this.list = list;
        DragGestureRecognizer dgr = ds.createDefaultDragGestureRecognizer(list,
                DnDConstants.ACTION_MOVE, this);
    }
    
    @Override
    public void dragGestureRecognized(final DragGestureEvent dge) {
        
        Object selectedData = list.getSelectedValue();
        int selectedIndex = list.getSelectedIndex();
        
        TransferItem transferItem = TransferItem.builder()
                .index(selectedIndex)
                .data(selectedData)
                .build();
        
        TransferableItem transferable = new TransferableItem(transferItem);
        
        ds.startDrag(dge, DragSource.DefaultCopyDrop, transferable, this);
    }

    @Override
    public void dragEnter(final DragSourceDragEvent dsde) {
    }

    @Override
    public void dragExit(final DragSourceEvent dse) {
    }

    @Override
    public void dragOver(final DragSourceDragEvent dsde) {
    }

    @Override
    public void dragDropEnd(final DragSourceDropEvent dsde) {
        if (!dsde.getDropSuccess()) {
            System.out.println("Failed");
        }
    }

    @Override
    public void dropActionChanged(final DragSourceDragEvent dsde) {
    }
}
